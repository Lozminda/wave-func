import sys
import os
import pygame
from pygame.locals import *

import random

# import numpy as np
# import array

class Tiles():
    '''
    Each tile has a name and 4 attributes (for each side) assosiated
    with it. It also has a pygame surface to for blitting. S
    and attributes are brought together in set_up_tiles.
    Tiles.calc_partners() works out how many and what tiles partner with
    tile, for example l1, mates with on its top edge :cross, l1, rc1, r4,
    t1, t2, t4. On left(MIGHT ACTUALLY BE RIGHT) hand side:blank, l1,  rc3, rc4, t4.
    Bottom edge: cross, l1, rc2, rc3, t2, t3, t4. And finally right (LEFT ??) hand
    side edge:blank, l1, rc1, rc2, t2
    '''

    # names 'allignes with' tile attributes,  each name having four attributes
    # tilenames + .png give the file name of their png file in Tiles/ folder
    names = tuple(('blank','cross','l1','l2','rc1',
                    'rc2','rc3','rc4', 't1','t2','t3','t4'))


    # Each tile edge is divided into three, with 0 => white and 1 => black
    # the first attribute (ie tile_attribute[0]) is for tilename[0] = blank
    # Is this the best way of doing this ?
    attributes = tuple((
                            ((0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)),
                            ((0, 1, 0), (0, 1, 0), (0, 1, 0), (0, 1, 0)),
                            ((0, 1, 0), (0, 0, 0), (0, 1, 0), (0, 0, 0)),
                            ((0, 0, 0), (0, 1, 0), (0, 0, 0), (0, 1, 0)),
                            ((0, 0, 0), (0, 0, 0), (0, 1, 0), (0, 1, 0)),
                            ((0, 1, 0), (0, 0, 0), (0, 0, 0), (0, 1, 0)),
                            ((0, 1, 0), (0, 1, 0), (0, 0, 0), (0, 0, 0)),
                            ((0, 0, 0), (0, 1, 0), (0, 1, 0), (0, 0, 0)),
                            ((0, 0, 0), (0, 1, 0), (0, 1, 0), (0, 1, 0)),
                            ((0, 1, 0), (0, 0, 0), (0, 1, 0), (0, 1, 0)),
                            ((0, 1, 0), (0, 1, 0), (0, 0, 0), (0, 1, 0)),
                            ((0, 1, 0), (0, 1, 0), (0, 1, 0), (0, 0, 0))
                            ))
    graphics_tiles = []
    TILE_SIZE_PXLS = 54
    tile_counter = 0

    def __init__(self, tilename, attribute):
        # number is essentially the index of the names, thus blank has number 0
        # l2 is 2 etc. It's easier to deal with a number rather than a string in the
        # screen_array in WaveFunctionCollapser()
        self.number = Tiles.tile_counter

        # As tiles are assigned in order of names tile_counter is assigning tiles
        # their number as they're initialisated
        Tiles.tile_counter += 1

        # Each side (top right bottom left) will have a list of possible partners,  which
        # will be sorted in calc_partners()
        self.partners = [[],[],[],[]]

        self.attributes = attribute
        filename = "Tiles/" + tilename + ".png"
        self.png_image = None

        try:
            self.png_image = pygame.image.load(os.path.join("./", filename))
        except FileNotFoundError:
            print("File not found at tile iniit: " + filename)
        except pygame.error:
            print("There's a pygame error in initialising a tile: ", pygame.geterror())
        except Exception:
            print("There's some other error other than a pygame error in tile initialisation:" + filename)
        else:
            self.tilename = tilename

        self.tilename = tilename
        self.calc_partners()
        return None

    def calc_partners(self):
        '''
        Calulates the partner tiles for each side of the current tile i.e. self
        by 'looking at' the attributes of each side and pairing them up with the reversed
        attributes of all the other tiles on the appropriate side,  so it matches attributes
        of top to bottom,  left to right etc
        '''

        # top => bottom of other tile,  index 0 => 2
        # left 1, to right 3
        # 2 => 0
        # 3 => 1 plus 2 mod 4 gives index
        # for idx, x in enumerate(xs):
        which_side = (2, 3, 0, 1)

        # side is the index and attributes_of_one_side are the current attributes on the
        # side of the tile that's under inspection:
        # thus side should go from 0 to 3, top, left, bottom, right
        for side, attributes_of_one_side in enumerate(self.attributes):

            # this loop goes through all the attributes,  defined at top of the program
            for name, attribute in zip(Tiles.names, Tiles.attributes):

                # if we're looking at the top of the tile then we want the bottom
                # attributes reversed and compares this against the appropriate
                # attribute side. If there's a match po the name into self.parteners
                temp = reversed(attribute[which_side[side]])
                if tuple(temp) == attributes_of_one_side:
                        self.partners[side].append(name)

    @classmethod
    def set_up_tiles(cls):
        for a_tile, an_attribute in zip(Tiles.names, Tiles.attributes):
            a_temptile = Tiles(a_tile, an_attribute)
            cls.graphics_tiles.append(a_temptile)

class WaveFunctionCollapser():
    def __init__(self, screen_w, screen_h, size, total_tiles ):
        self.screen_w = screen_w
        self.screen_h = screen_h
        self.array_h = screen_h // size
        self.array_w = screen_w // size
        self.screen_array = [[0 for i in range(self.array_w)] for j in range(self.array_h)]
        # Could use numpy ndarray later:-
        # self.screen_array = np.zeros((self.array_h, self.array_w), dtype = np.int8)
        self.size = size
        self.array_x = 0
        self.array_y = 0
        self.total_tiles = total_tiles
        self.type = 0

    def next_tile(self, all_the_tiles):

        TOP = 0
        RIGHTSIDE = 1
        BOTTOM = 2
        LEFTSIDE = 3

        BLANK_TILE = 0

        if self.array_x < (self.array_w):

            # find parners to the right
            # find parners underneath
            # if that group is larger than 1 pick one from that group

            # previous_tile_to_left = 0
            # previous_tile_above = 0

            previous_tile_to_left = self.screen_array[self.array_y][self.array_x - 1]
            previous_tile_above = self.screen_array[self.array_y - 1][self.array_x]

            temp_partners_bottom = all_the_tiles[previous_tile_above].partners[BOTTOM]
            temp_partners_rhs = all_the_tiles[previous_tile_to_left].partners[RIGHTSIDE]

            if ((self.array_y == 0) and (self.array_x == 0)):        # ie the top row
                temp_partners_bottom = all_the_tiles[BLANK_TILE].partners[BOTTOM]
                temp_partners_rhs = all_the_tiles[BLANK_TILE].partners[RIGHTSIDE]
            elif self.array_x == 0:
                temp_partners_rhs = all_the_tiles[BLANK_TILE].partners[RIGHTSIDE]
            elif self.array_y == 0:        # ie the top row
                temp_partners_bottom = all_the_tiles[BLANK_TILE].partners[BOTTOM]

            # if we're in top right corner then there's a three way intersection
            if self.array_x == self.array_w - 1:
                temp_partners_LHS = all_the_tiles[BLANK_TILE].partners[LEFTSIDE]
                temp_partners_bottom = list(set(temp_partners_LHS).intersection(temp_partners_bottom))

            if self.array_y == self.array_h - 2:
                temp_partners_TOP = all_the_tiles[BLANK_TILE].partners[TOP]
                temp_partners_bottom = list(set(temp_partners_TOP).intersection(temp_partners_bottom))

            possible_tiles = list(set(temp_partners_rhs).intersection(temp_partners_bottom))
            num_of_possibles = len(possible_tiles)

            # if there's no choices back up a couple of tiles or a line up and try again
            if num_of_possibles == 0:
                if self.array_x > 2:
                    self.array_x -= 2
                else:
                    if self.array_y > 0:
                        self.array_y -= 1
                    else:
                        print("Houston we have a problem: No where to back up wednesday!")
                        input('Press Enter')
                        sys.exit()
                return self.next_tile(all_the_tiles)
            elif num_of_possibles != 1:
                the_choice = random.randint(0, (num_of_possibles - 1))
                tilename = possible_tiles[the_choice]
            else:
                # without the index[0] tilename was wrong type and thus
                # self.type = a_tile.number wasn't working later
                tilename = possible_tiles[0]

            for a_tile in all_the_tiles:
                if a_tile.tilename == str(tilename):
                    self.type = a_tile.number
                    break
            else:
                print("Houston we have a problem")
                input('Press Enter')
                sys.exit()

            # really shouldn't have an indexing error here !
            try:
                self.screen_array[self.array_y][self.array_x] = self.type
            except IndexError:
                print('self.array_x:',self.array_x)
                print('self.array_y:',self.array_y)
                input('Press Enter')
                sys.exit()

            x = self.array_x * self.size
            y = self.array_y * self.size
            self.array_x += 1

            return {'type': self.type, 'x': x, 'y': y, 'done':False }
        else:
            #Handle borders
            self.array_x = 0
            self.array_y += 1
            if self.array_y >= self.array_h:
                self.array_x = 0
                self.array_y = 0
                return {'type': 0, 'x': 0, 'y': 0, 'done':True }
            # do the new calculation at new place to eventually return the dictionary
            # at line 221
            return self.next_tile(all_the_tiles)

try:
    Tiles.set_up_tiles()
except Exception:
    print("Oh no disaster setting up tiles!")

pygame.init()  # initialize pygame
screen = pygame.display.set_mode()
screen = pygame.display.set_mode(screen.get_size())
pygame.display.set_caption('Wave Function Collapse')
screen_w, screen_h = pygame.display.get_surface().get_size()

# screen_w = 1366
# screen_h = 768
wfc = WaveFunctionCollapser(screen_w, screen_h, Tiles.TILE_SIZE_PXLS,
                            len(Tiles.names))
newtile = wfc.next_tile(Tiles.graphics_tiles)
screen.blit(Tiles.graphics_tiles[newtile['type']].png_image, (newtile['x'], newtile['y']))

admiring_finished_wfc = False
running = True
while running:
    if not newtile['done']:
        newtile = wfc.next_tile(Tiles.graphics_tiles)
        screen.blit(Tiles.graphics_tiles[newtile['type']].png_image, (newtile['x'], newtile['y']))
        pygame.display.update()
    else:
        admiring_finished_wfc = True

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                running = False
            if event.key == K_SPACE and admiring_finished_wfc:
                admiring_finished_wfc = False
                newtile['done'] = False
pygame.quit()